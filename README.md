# TestPay

Implementation of self-contained testing environment (sandbox) that mimics the live production environment for fictional payment system (TestPay) .


Requests for curl:

Get token (client_id=client_id; secret=secret): 
curl -v http://localhost:8089/oauth2/token -H "Accept: application/json" -H "Accept-Language: en_US" -u "client_id:secret" -d "grant_type=client_credentials" -k

Get data (change token for yours):
curl -v http://localhost:8089/payments/payment -H "Content-Type: application/json" -H "Authorization: Bearer aa8eb017-a676-4da8-b26e-465598bd9cc6" -d "{\"intent\" : \"order\",\"notification_url\": \"https://requestb.in/1i063pj1\",\"payer\": {\"email\": \"test@example.com\"},\"transaction\": {\"external_id\": \"123456789\",\"amount\": {\"value\": \"7.47\",\"currency\": \"USD\"},\"description\": \"The payment transaction description\"}}" -k


Instruction for generating SSL certificate:
https://www.thomasvitale.com/https-spring-boot-ssl-certificate/

For proper work of curl requests must be done one of following:
- SSL certificate verification should be disabled; 
or
- the certificate in the resource folder should be added as trusted:
See: https://curl.haxx.se/docs/sslcerts.html
