package com.splat.applyforjob.testpay.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.splat.applyforjob.testpay.model.rest.response.ErrorResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {

    private static Logger logger = LogManager.getLogger();

    @Override
    public void commence(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse,
                         AuthenticationException e) throws IOException, ServletException {

        logger.debug("AUTHENTIFICATION_FAILURE");

        httpServletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);
        httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

        ErrorResponse errorResponse = ErrorResponse.builder()
                .error("AUTHENTIFICATION_FAILURE")
                .errorDescription("Authentication failed due to invalid authentication credentials")
                .build();

        String jsonErrorResponce = new ObjectMapper().writeValueAsString(errorResponse);
        httpServletResponse.getWriter().write(jsonErrorResponce);
    }
}
