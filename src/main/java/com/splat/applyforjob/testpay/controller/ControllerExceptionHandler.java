package com.splat.applyforjob.testpay.controller;

import com.splat.applyforjob.testpay.exception.RequestValidationException;
import com.splat.applyforjob.testpay.model.rest.response.ErrorResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerExceptionHandler {

    private static Logger logger = LogManager.getLogger();

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ErrorResponse> handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {

        logger.debug("BAD REQUEST: \n"+ e);

        ErrorResponse errorResponse = ErrorResponse.builder()
                .error("INVALID_REQUEST")
                .errorDescription("Request is not well-formatted, syntactically incorrect or violates schema")
                .build();
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<ErrorResponse> handleNullPointerException(NullPointerException e) {

        logger.debug("BAD REQUEST: \n"+ e);

        ErrorResponse errorResponse = ErrorResponse.builder()
                .error("INVALID REQUEST")
                .errorDescription("Request is not well-formatted, syntactically incorrect or violates schema")
                .build();
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RequestValidationException.class)
    public ResponseEntity<ErrorResponse> handleRequestValidationException(RequestValidationException e) {

        logger.debug("BAD REQUEST: \n"+ e);

        ErrorResponse errorResponse = ErrorResponse.builder()
                .error("INVALID_REQUEST")
                .errorDescription("Request is not well-formatted, syntactically incorrect or violates schema")
                .build();
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseEntity<ErrorResponse> handleHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException e) {

        logger.debug("UNSUPPORTED MEDIA TYPE: \n"+ e);

        ErrorResponse errorResponse = ErrorResponse.builder()
                .error("UNSUPPORTED_MEDIA_TYPE")
                .errorDescription("The server does not support the request payload media type")
                .build();
        return new ResponseEntity<>(errorResponse, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleException(Exception e) {

        logger.debug("INTERNAL SERVER ERROR: \n"+ e);

        ErrorResponse errorResponse = ErrorResponse.builder()
                .error("INTERNAL_SERVER_ERROR")
                .errorDescription("An internal server error has occurred")
                .build();
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
