package com.splat.applyforjob.testpay.controller;

import com.splat.applyforjob.testpay.exception.RequestValidationException;
import com.splat.applyforjob.testpay.model.rest.request.PaymentRequest;
import com.splat.applyforjob.testpay.model.rest.response.PaymentResponse;
import com.splat.applyforjob.testpay.service.api.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @PostMapping(value = "/payments/payment",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public PaymentResponse createPayment(@RequestBody @Validated PaymentRequest paymentRequest,
                                         BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new RequestValidationException("Request Validation Error");
        }

        return paymentService.process(paymentRequest);
    }
}
