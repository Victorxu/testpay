package com.splat.applyforjob.testpay.service.impl;

import com.splat.applyforjob.testpay.model.entity.Payment;
import com.splat.applyforjob.testpay.model.entity.State;
import com.splat.applyforjob.testpay.model.rest.response.Notification;
import com.splat.applyforjob.testpay.service.api.PaymentService;
import com.splat.applyforjob.testpay.service.api.TaskSimulator;
import com.splat.applyforjob.testpay.service.api.Webhook;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.util.Random;

import static com.splat.applyforjob.testpay.util.PaymentTransformer.toNotification;

@Service
public class TaskSimulatorImpl implements TaskSimulator {

    private static Logger logger = LogManager.getLogger();

    @Autowired
    private Webhook webhook;

    @Autowired
    private PaymentService paymentService;

    @Override
    @Async
    public void simulatePaymentTask(Payment payment) {
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            logger.debug("simulatePaymentTask thread interrupted");
        }
        // just the probabilities, nothing personal
        if (new Random().nextInt(10) != 0) {
            payment.setState(State.APPROVED);
        } else {
            payment.setState(State.FAILED);
        }

        Payment processedPayment = paymentService.save(payment);

        Notification notification = toNotification(processedPayment);
        URI uri = URI.create(payment.getNotificationUrl());
        webhook.send(notification, uri);
    }
}
