package com.splat.applyforjob.testpay.service.impl;

import com.splat.applyforjob.testpay.dao.api.PaymentDao;
import com.splat.applyforjob.testpay.model.entity.Payment;
import com.splat.applyforjob.testpay.model.rest.request.PaymentRequest;
import com.splat.applyforjob.testpay.model.rest.response.PaymentResponse;
import com.splat.applyforjob.testpay.service.api.PaymentService;
import com.splat.applyforjob.testpay.service.api.TaskSimulator;

import static com.splat.applyforjob.testpay.util.PaymentTransformer.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private TaskSimulator taskSimulator;

    @Autowired
    private PaymentDao paymentDao;

    @Override
    public PaymentResponse process(PaymentRequest paymentRequest) {
        Payment payment = toPayment(paymentRequest);
        payment = save(payment);
        PaymentResponse paymentResponse = toPaymentResponse(payment);

        taskSimulator.simulatePaymentTask(payment);

        return paymentResponse;
    }

    @Transactional
    @Override
    public Payment save(Payment payment) {
        return paymentDao.save(payment);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Payment> getAll() {
        return paymentDao.findAll();
    }
}
