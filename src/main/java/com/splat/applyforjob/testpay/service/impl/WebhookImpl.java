package com.splat.applyforjob.testpay.service.impl;

import com.splat.applyforjob.testpay.model.rest.response.Notification;
import com.splat.applyforjob.testpay.service.api.Webhook;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.time.Instant;
import java.util.Formatter;

@Service
public class WebhookImpl implements Webhook {

    private static Logger logger = LogManager.getLogger();

    private final static long THREE_DAYS_IN_SECONDS = 60 * 60 * 24 * 3;

    // timeout between webhook notification attempts = 1 minute (milliseconds)
    private final static long TIMEOUT = 60 * 1000;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public void send(Notification notification, URI uri) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        RequestEntity<Notification> requestEntity =
                new RequestEntity<>(notification, headers, HttpMethod.POST, uri);


        Instant startPlusThreeDays = Instant.now().plusSeconds(THREE_DAYS_IN_SECONDS);
        int attempts = 0;

        // Trying to send notification until:
        //      success
        //   OR 25 attempts
        //   OR three days since first attempt
        while ((Instant.now().isBefore(startPlusThreeDays)) && (attempts < 25)) {

            attempts++;
            try {
                ResponseEntity<Object> responseEntity = restTemplate.exchange(requestEntity, Object.class);
                if (responseEntity.getStatusCode().is2xxSuccessful()) {
                    logger.info ("Webhook listener received notification! Response: " + responseEntity);
                    break;
                } else {
                    logger.debug ("Http response is not 200-OK" + responseEntity);
                    throw new RuntimeException("Http response is not 200-OK");
                }
            } catch (Exception e) {
                Formatter formatter = new Formatter();
                logger.info(formatter.format("Webhook listener did not receive notification %d times: ", attempts)+ e.toString());
            }

            try {
                Thread.sleep(TIMEOUT);
            } catch (InterruptedException e) {
                logger.debug("webhook sending task interrupted");
            }
        }
    }
}
