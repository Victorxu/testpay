package com.splat.applyforjob.testpay.service.api;

import com.splat.applyforjob.testpay.model.rest.response.Notification;

import java.net.URI;

public interface Webhook {
    void send(Notification notification, URI uri);
}
