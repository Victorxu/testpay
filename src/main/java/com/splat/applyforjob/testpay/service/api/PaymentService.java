package com.splat.applyforjob.testpay.service.api;

import com.splat.applyforjob.testpay.model.entity.Payment;
import com.splat.applyforjob.testpay.model.rest.request.PaymentRequest;
import com.splat.applyforjob.testpay.model.rest.response.PaymentResponse;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PaymentService {
    PaymentResponse process(PaymentRequest paymentRequest);
    Payment save(Payment payment);
    List<Payment> getAll();
}
