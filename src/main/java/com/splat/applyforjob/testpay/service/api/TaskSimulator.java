package com.splat.applyforjob.testpay.service.api;

import com.splat.applyforjob.testpay.model.entity.Payment;

public interface TaskSimulator {
    void simulatePaymentTask(Payment payment);
}
