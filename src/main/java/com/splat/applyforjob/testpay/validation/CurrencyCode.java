package com.splat.applyforjob.testpay.validation;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Size;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * The three-character ISO-4217 currency code.
 */

@Target(FIELD)
@Retention(RUNTIME)
@Constraint(validatedBy = {CurrencyCodeValidator.class})
@Documented
@NotBlank
@Size(min = 3, max = 3)
public @interface CurrencyCode {

    String message() default "Not a valid currency code";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
