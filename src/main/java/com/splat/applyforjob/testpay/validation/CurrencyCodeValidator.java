package com.splat.applyforjob.testpay.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Currency;

public class CurrencyCodeValidator implements ConstraintValidator<CurrencyCode, String> {

    @Override
    public void initialize(CurrencyCode currencyCode) {
    }

    @Override
    public boolean isValid(String currency, ConstraintValidatorContext context) {

        try {
            Currency.getInstance(currency);
        } catch (IllegalArgumentException | NullPointerException e) {
            return false;
        }
        return true;
    }
}
