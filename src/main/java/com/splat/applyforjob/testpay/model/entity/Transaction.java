package com.splat.applyforjob.testpay.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class Transaction {

    @JsonProperty("external_id")
    private String externalId;

    @NotNull
    @Valid
    private Amount amount;

    private String description;
}

