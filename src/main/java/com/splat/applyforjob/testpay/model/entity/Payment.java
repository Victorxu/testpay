package com.splat.applyforjob.testpay.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Table(name = "payment")
@Data
@EqualsAndHashCode(exclude = {"id", "state"})
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Payment {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column
    private Intent intent;

    @Column
    private String notificationUrl;

    @Column
    private String email;

    @Column
    private String externalId;

    @Column
    private String value;

    @Column
    private String currency;

    @Column
    private String description;

    @Column
    private State state;

    @Column
    private Instant createTime;

}
