package com.splat.applyforjob.testpay.model.rest.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.splat.applyforjob.testpay.model.entity.State;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class PaymentResponse implements Serializable {

    private String id;

    @JsonProperty("create_time")
    private String createTime;

    private State state;
}

