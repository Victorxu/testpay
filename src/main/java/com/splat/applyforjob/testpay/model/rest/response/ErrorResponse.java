package com.splat.applyforjob.testpay.model.rest.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class ErrorResponse implements Serializable {

    private String error;

    @JsonProperty("error_description")
    private String errorDescription;
}
