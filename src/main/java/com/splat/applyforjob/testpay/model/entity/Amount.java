package com.splat.applyforjob.testpay.model.entity;

import com.splat.applyforjob.testpay.validation.CurrencyCode;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Digits;
import org.hibernate.validator.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class Amount {

    @NotBlank
    @Size(max = 10)
    @Digits(integer = 10, fraction = 2)
    private String value;


    @CurrencyCode
    private String currency;
}
