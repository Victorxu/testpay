package com.splat.applyforjob.testpay.model.rest.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.splat.applyforjob.testpay.model.entity.State;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;

@Data
@Builder
public class Notification implements Serializable {

    private String currency;
    private String amount;
    private String id;

    @JsonProperty("external_id")
    private String externalId;
    private State status;
    final private String sha2sig = sha256Hex("secret word of merchant account");
}
