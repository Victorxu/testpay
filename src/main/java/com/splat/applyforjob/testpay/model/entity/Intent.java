package com.splat.applyforjob.testpay.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Intent {

    @JsonProperty("order")
    ORDER
}
