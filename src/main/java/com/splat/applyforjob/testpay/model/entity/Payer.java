package com.splat.applyforjob.testpay.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Email;

@Data
@NoArgsConstructor
public class Payer {

    @NotBlank
    @Email
    private String email;
}
