package com.splat.applyforjob.testpay.model.rest.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.splat.applyforjob.testpay.model.entity.Intent;
import com.splat.applyforjob.testpay.model.entity.Payer;
import com.splat.applyforjob.testpay.model.entity.Transaction;
import lombok.Data;

import javax.validation.Valid;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class PaymentRequest implements Serializable{

    @NotNull
    private Intent intent;

    @NotBlank
    @URL
//    @Pattern(regexp = "^(https?):\\/\\/[-a-zA-Z0-9+&@#\\/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#\\/%=~_|]")
    @JsonProperty("notification_url")
    private String notificationUrl;

    @NotNull
    @Valid
    private Payer payer;

    @NotNull
    @Valid
    private Transaction transaction;
}
