package com.splat.applyforjob.testpay.dao.api;

import com.splat.applyforjob.testpay.model.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * PaymentDao interface for database storage.
 */

public interface PaymentDao extends JpaRepository<Payment, String> {
}
