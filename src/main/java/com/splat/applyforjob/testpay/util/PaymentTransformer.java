package com.splat.applyforjob.testpay.util;

import com.splat.applyforjob.testpay.model.entity.Payment;
import com.splat.applyforjob.testpay.model.entity.State;
import com.splat.applyforjob.testpay.model.rest.request.PaymentRequest;
import com.splat.applyforjob.testpay.model.rest.response.Notification;
import com.splat.applyforjob.testpay.model.rest.response.PaymentResponse;

import java.time.Instant;
import java.time.format.DateTimeFormatter;

public class PaymentTransformer {

    public static Payment toPayment(PaymentRequest paymentRequest) {
        Instant instant = Instant.now();
        instant = instant.minusNanos(instant.getNano());

        return Payment.builder()
                .intent(paymentRequest.getIntent())
                .notificationUrl(paymentRequest.getNotificationUrl())
                .email(paymentRequest.getPayer().getEmail())
                .externalId(paymentRequest.getTransaction().getExternalId())
                .value(paymentRequest.getTransaction().getAmount().getValue())
                .currency(paymentRequest.getTransaction().getAmount().getCurrency())
                .description(paymentRequest.getTransaction().getDescription())
                .state(State.CREATED)
                .createTime(instant)
                .build();
    }

    public static PaymentResponse toPaymentResponse(Payment payment) {
        return PaymentResponse.builder()
                .id(payment.getId())
                .createTime(DateTimeFormatter.ISO_INSTANT.format(payment.getCreateTime()))
                .state(payment.getState())
                .build();
    }

    public static Notification toNotification(Payment payment) {
        return Notification.builder()
                .id(payment.getId())
                .currency(payment.getCurrency())
                .amount(payment.getValue())
                .externalId(payment.getExternalId())
                .status(payment.getState())
                .build();
    }
}
